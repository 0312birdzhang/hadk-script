#!/usr/bin/bash
cat > $HOME/.hadk.env <<EOF
export MER_ROOT="/home/$USER/mer"
export ANDROID_ROOT="$MER_ROOT/android/droid"
export VENDOR="sony"
export DEVICE="maple"
export PORT_ARCH="arm64"
EOF
cat > $HOME/.mersdkubu.profile <<EOF
function hadk() { source $HOME/.hadk.env; echo "Env setup for $DEVICE"; }
export PS1="HABUILD_SDK [\${DEVICE}] $PS1"
hadk
EOF
cat > $HOME/.mersdk.profile <<EOF
function hadk() { source $HOME/.hadk.env; echo "Env. setup for $DEVICE"; }
hadk
EOF
export MER_ROOT=$HOME/mer
TARBALL=mer-i486-latest-sdk-rolling-chroot-armv7hl-sb2.tar.bz2
curl -k -O https://img.merproject.org/images/mer-sdk/$TARBALL
sudo mkdir -p $MER_ROOT/sdks/sdk
cd $MER_ROOT/sdks/sdk
sudo tar --numeric-owner -p -xjf $HOME/$TARBALL
echo "export MER_ROOT=$MER_ROOT" >> ~/.bashrc
echo 'alias sdk=$MER_ROOT/sdks/sdk/mer-sdk-chroot' >> ~/.bashrc
exec bash
echo 'PS1="MerSDK $PS1"' >> ~/.mersdk.profile
cd $HOME
sdk
exit
sdk
sudo zypper ar http://repo.merproject.org/obs/home:/sledge:/mer/latest_i486/ \
curlfix
sudo zypper ar http://repo.merproject.org/obs/nemo:/devel:/hw:/common/sailfish_latest_armv7hl/ \
common 
sudo zypper ref curlfix
sudo zypper dup --from curlfix
sudo zypper ref common
sudo zypper dup --from common
sudo zypper in android-tools createrepo zip
TARBALL=ubuntu-trusty-android-rootfs.tar.bz2
curl -O http://img.merproject.org/images/mer-hybris/ubu/$TARBALL
UBUNTU_CHROOT=$MER_ROOT/sdks/ubuntu
sudo mkdir -p $UBUNTU_CHROOT
sudo tar --numeric-owner -xvjf $TARBALL -C $UBUNTU_CHROOT
ubu-chroot -r $MER_ROOT/sdks/ubuntu
exit
echo git config --global user.name "Kristoffer Grundström"
git config --global user.email "hamnisdude@gmail.com"
mkdir ~/bin
PATH=~/bin:$PATH
curl https://storage.googleapis.com/git-repo-downloads/repo > ~/bin/repo
chmod a+x ~/bin/repo
ubu-chroot -r $MER_ROOT/sdks/ubuntu
sudo mkdir -p $ANDROID_ROOT
sudo chown -R $USER $ANDROID_ROOT
cd $ANDROID_ROOT
repo init -u git://github.com/mer-hybris/android.git -b hybris-15.1
mkdir $ANDROID_ROOT/.repo/local_manifests
cd $HOME/.repo/local_manifests/
touch maple.xml
cat > maple.xml <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<manifest>
<project path="device/sony/scorpion" name="CurveOS/android_device_sony_maple" revision="lineage-15.1" />
<project path="device/sony/shinano-common" name="CurveOS/android_device_sony_shinano-common" revision="lineage-15.1" />
<project path="device/sony/msm8974-common" name="CurveOS/android_device_sony_msm8974-common" revision="lineage-15.1" />
<project path="device/sony/common" name="CurveOS/android_device_sony_common" revision="lineage-15.1" />
<project path="device/qcom/common" name="CurveOS/android_device_qcom_common" revision="lineage-15.1" />
<project path="hardware/sony/thermanager" name="CurveOS/android_hardware_sony_thermanager" revision="lineage-15.1" />
<project path="kernel/sony/msm8998" name="cryptomilk/android_kernel_sony_msm8998" revision="hybris-15.1" />
<project path="vendor/sony" name="TheMuppets/proprietary_vendor_sony" revision="lineage-15.1" />
<project path="rpm/" name="CurveOS/droid-hal-scorpion" revision="master" />
<project path="hybris/droid-configs" name="CurveOS/droid-config-scorpion" revision="master" />
<project path="hybris/droid-hal-version-scorpion" name="CurveOS/droid-hal-version-scorpion" revision="master" />
</manifest>
EOF
cd $ANDROID_ROOT
repo sync --fetch-submodules
##Rerun again to make sure that you're fully updated.
repo sync --fetch-submodules
cat > /home/$USER/mer/android/droid/hybris/hybris-boot/fixup-mountpoints << EOF
#!/bin/bash
# Fix up mount points device node names.
# This is broken pending systemd > 191-2 so hack the generated unit files :(
# See: https://bugzilla.redhat.com/show_bug.cgi?id=859297

DEVICE=$1
shift

echo "Fixing mount-points for device $DEVICE"

case "$DEVICE" in
    
    "scorpion")
         sed -i \
             -e 's block/platform/soc/1da4000.ufshc/by-name/FOTAKernel sde45 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/LTALabel sda2 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/Qnovo sda11 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/TA sda1 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/abl sde9 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/ablbak sde10 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/apdp sde38 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/apps_log sda9 ' \
             -e 's block platform/soc/1da4000.ufshc/by-name/bluetooth/sde37 '\
             -e 's block/platform/soc/1da4000.ufshc/by-name/boot sde35 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/cache sda5 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/cmnlib sde14 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/cmnlib64 sde16 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/cmnlib64bak sde17 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/cmnlibbak sde15 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/ddr sdd1 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/devcfg sde19 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/devcfgbak sde20 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/devinfo sde11 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/diag sda10 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/dpo sde40 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/dsp sde34 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/frp sda8 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/fsc sdf3 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/fsg sde31 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/fsmetadata sda12 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/hyp sde5 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/hypbak sde6 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/keymaster sde12 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/keymasterbak sde13 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/keystore sda7 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/limits sde42 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/logfs sde44 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/misc sda6 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/modem sde33 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/modemst1 sdf1 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/modemst2 sdf2 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/msadp sde39 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/oem sda14 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/persist sda4 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/pmic sde7 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/pmicbak sde8 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/rdimage sda13 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/recovery sde36 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/rpm sde1 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/rpmbak sde2 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/sec sde32 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/splash sde41 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/ssd sda3 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/sti sde18 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/storsec sde21 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/storsecbak sde22 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/system sde46 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/toolsfv sde43 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/tz sde3 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/tzbak sde4 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/tzxfl sde27 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/tzxflattest sde25 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/tzxflattestbak sde26 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/tzxflbak sde28 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/userdata sda15 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/xbl sdb1 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/xblbak sdc1 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/xfl sde23 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/xflbak sde24 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/xflkeystore sde29 ' \
             -e 's block/platform/soc/1da4000.ufshc/by-name/xflkeystorebak sde30 ' \
            "$@" ;;

     *)
        exit 1
        ;;
esac
EOF
source build/envsetup.sh
export USE_CCACHE=1
breakfast $DEVICE
make -j4 hybris-hal
##This will fail, but run this to solve that and rerun make -j4 hybris-hal again and you should be fine.
java -jar /home/$USER/mer/android/droid/out/host/linux-x86/framework/dumpkey.jar build/target/product/security/testkey.x509.pem build/target/product/security/cm.x509.pem build/target/product/security/cm-devkey.x509.pem > /home/$USER/mer/android/droid/out/target/product/scorpion/obj/PACKAGING/ota_keys_intermediates/keys
make -j4 hybris-hal
hybris/mer-kernel-check/mer_verify_kernel_config \
./out/target/product/$DEVICE/obj/KERNEL_OBJ/.config
make hybris-boot && make hybris-recovery
exit
SFE_SB2_TARGET=$MER_ROOT/targets/$VENDOR-$DEVICE-$PORT_ARCH
TARBALL_URL=http://releases.sailfishos.org/sdk/latest/targets/targets.json
TARBALL=$(curl $TARBALL_URL | grep "$PORT_ARCH.tar.bz2" | cut -d\" -f4)
curl -O $TARBALL
sudo mkdir -p $SFE_SB2_TARGET
sudo tar --numeric-owner -pxjf $(basename $TARBALL) -C $SFE_SB2_TARGET
sudo chown -R $USER $SFE_SB2_TARGET
cd $SFE_SB2_TARGET
grep :$(id -u): /etc/passwd >> etc/passwd
grep :$(id -g): /etc/group >> etc/group
sb2-init -d -L "--sysroot=/" -C "--sysroot=/" \
-c /usr/bin/qemu-arm-dynamic -m sdk-build \
-n -N -t / $VENDOR-$DEVICE-$PORT_ARCH \
/opt/cross/bin/$PORT_ARCH-meego-linux-gnueabi-gcc
sb2 -t $VENDOR-$DEVICE-$PORT_ARCH -m sdk-install -R rpm --rebuilddb
sb2 -t $VENDOR-$DEVICE-$PORT_ARCH -m sdk-install -R zypper ar \
-G http://repo.merproject.org/releases/mer-tools/rolling/builds/$PORT_ARCH/packages/ \
mer-tools-rolling
sb2 -t $VENDOR-$DEVICE-$PORT_ARCH -m sdk-install -R zypper ref --force
cd $HOME
cat > main.c << EOF
#include <stdlib.h>
#include <stdio.h>
int main(void) {
printf("Hello, world!\n");
return EXIT_SUCCESS;
}
EOF
sb2 -t $VENDOR-$DEVICE-$PORT_ARCH gcc main.c -o test
sb2 -t $VENDOR-$DEVICE-$PORT_ARCH ./test
sudo zypper ref; sudo zypper dup
cd $HOME
sudo mkdir -p $MER_ROOT/devel
sudo chown -R $USER mer/devel
sb2 -t $VENDOR-$DEVICE-$PORT_ARCH -R -m sdk-install ssu ar common http://repo.merproject.org/obs/nemo:/devel:/hw:/common/sailfish_latest_armv7hl/
cd $ANDROID_ROOT
rpm/dhd/helpers/build_packages.sh
##Press Ctrl C to stop this process if you at some point end up with what looks as a freeze and rerun rpm/dhd/helpers/build_packages.sh
mkdir -p tmp
HA_REPO="repo --name=adaptation0-$DEVICE-@RELEASE@"
KS="Jolla-@RELEASE@-$DEVICE-@ARCH@.ks"
sed -e "s|^$HA_REPO.*$|$HA_REPO --baseurl=file://$ANDROID_ROOT/droid-local-repo/$DEVICE|" $ANDROID_ROOT/hybris/droid-configs/installroot/usr/share/kickstarts/$KS > tmp/$KS
RELEASE=2.1.4.14
EXTRA_NAME=-my1
sudo mic create fs --arch $PORT_ARCH \
--debug \
--runtime=native \
--tokenmap=ARCH:$PORT_ARCH,RELEASE:$RELEASE,EXTRA_NAME:$EXTRA_NAME \
--record-pkgs=name,url \
--outdir=sfe-$DEVICE-$RELEASE$EXTRA_NAME \
--pack-to=sfe-$DEVICE-$RELEASE$EXTRA_NAME.tar.bz2 \
$ANDROID_ROOT/tmp/Jolla-@RELEASE@-$DEVICE-@ARCH@.ks
